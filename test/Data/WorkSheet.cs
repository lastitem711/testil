﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace test.Data
{
    public class WorkSheet
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле 'Имя' не может быть пустым")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Поле 'Фамилия' не может быть пустым")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Поле 'Язык программирования' не может быть пустым")]
        public string Language { get; set; }

        [Required(ErrorMessage = "Поле 'Возраст' не может быть пустым")]
        public DateTime Birth  { get; set; }

        [Required(ErrorMessage = "Поле 'Опыт' не может быть пустым")]
        public int Seniority { get; set; }



    }
}
