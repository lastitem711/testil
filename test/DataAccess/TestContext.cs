﻿using Microsoft.EntityFrameworkCore;
using test.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test.DataAccess
{
    public class TestContext : DbContext
    {
        public TestContext(DbContextOptions options) : base(options) 
        {
            Database.EnsureCreated();
        }

        public DbSet<WorkSheet> WorkSheets { get; set; }
    }
}
